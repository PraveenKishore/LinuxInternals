#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

using namespace std;

int main(int argc, char *argv[]) {
	if(argc == 3) {
		if(rename(argv[1], argv[2]) != -1) {
			cout << "Rename successful " << endl;
		} else {
			cout << "Rename failed! " << endl;
			return -1;
		}
	} else {
		cout << "Invalid arguments!" << endl << "Usage: mv <source> <destinaiton>" << endl;
	}
	return 0;
}
