#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

using namespace std;

int main(int argc, char *argv[]) {
	struct stat st;
	struct passwd *pw;
	struct group *gr;
	stat(argv[1], &st);
	pw = getpwuid(st.st_uid);
	gr = getgrgid(st.st_gid);
	cout << "File: " << argv[1] << endl;
	cout << "Size: " << st.st_size << " Blocks: " << st.st_blocks << " Block size: " << st.st_blksize << endl;
	cout << "File type: ";
	switch(st.st_mode & S_IFMT) {
		case S_IFREG: cout << "Regular" << endl; break;
		case S_IFDIR: cout << "Directory" << endl; break;
		case S_IFBLK: cout << "Block device" << endl; break;
		case S_IFCHR: cout << "Character device"<< endl; break;
		case S_IFLNK: cout << "Link" << endl; break;
		case S_IFIFO: cout << "Fifo" << endl; break;
	}
	cout << "Permission: ";
	char perm[10] = "rwxrwxrwx";
	for(int i = 0; i < 9; i++) {
		if((st.st_mode & (1 << i)) == 0) {
			perm[8-i] = '-';
		}
	}
	cout << perm << endl;
	cout << "Access time: " << ctime(&st.st_atime);
	cout << "Modify time: " << ctime(&st.st_mtime);
	cout << "Change time: " << ctime(&st.st_ctime);
	return 0;
}
