#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

using namespace std;

void lock(int);

int main(int argc, char *argv[]) {
	int fd;
	if(argc ==  2) {
		if((fd = open(argv[1], O_WRONLY, 0)) == -1) {
			perror("open");
			return -1;
		}
		lock(fd);
	}
	return 0;
}

void lock(int fd) {
	struct flock lock;
	int res;
	lock.l_type = F_WRLCK;
	lock.l_whence = 0;
	lock.l_start = 0;
	lock.l_len = 0;
	if((res = fcntl(fd, F_SETLK, &lock)) == -1) {
		perror("lock");
		return;
	}
	cout << "Locked the entire file successfully! " << endl;
	write(fd, "Hello world", 10);
	lock.l_type  = F_UNLCK;
	fcntl(fd, F_SETLK, &lock);
}
