#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

int main(int argc, char *argv[]) {
	int ret;
	if(argc == 3) {
		if(ret = link(argv[1], argv[2]) != -1) {
			perror("link");
			return -1;
		} else {
			cout << "Successfully created hard link" << endl;
		}
	} else if(argc == 4 && strcmp(argv[1], "-s") == 0) {
		if(ret = symlink(argv[2], argv[3]) != -1) {
			perror("symlink");
			return -1;
		} else {
			cout << "Successfully created soft link" << endl;
		}
	}
	else{
		cout << "Invalid arguments!" << endl << "Usage: ./a.out [-s] <source file> <destination link file>" << endl; 
	}
	return 0;
}
