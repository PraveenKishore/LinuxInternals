#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <sys/resource.h>

int main() {
	struct rlimit lim;
	getrlimit(RLIMIT_NPROC, &lim);
	printf("Current limit for number of child process: %lu, max: %lu\n", lim.rlim_cur, lim.rlim_max);
	// Increase the limit by 24
	lim.rlim_cur += 24;
	setrlimit(RLIMIT_NPROC, &lim);
	return 0;
}
